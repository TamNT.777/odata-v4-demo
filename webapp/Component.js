sap.ui.define([
	"sap/ui/model/odata/v4/ODataModel",
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"demo/odatav4/OdataV4/model/models"
], function (ODataModel, UIComponent, Device, models) {
	"use strict";

	return UIComponent.extend("demo.odatav4.OdataV4.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function () {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);
			// enable routing
			this.getRouter().initialize();

			// set the device model
			this.setModel(models.createDeviceModel(), "device");
		}
	});
});